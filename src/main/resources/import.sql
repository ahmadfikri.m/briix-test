--INSERT INTO post(id, title, content, tags) VALUES (1, 'Quarkus', 'Supersonic Subatomic Java', '["Java", "Framework"]');
--INSERT INTO post(id, title, content, tags) VALUES (2, 'Hibernate', 'Everything Data', '["Library"]');
--INSERT INTO post(id, title, content, tags) VALUES (3, 'Reactive', 'Reactive Application on The JVM', '["Java"]');

INSERT INTO post(id, title, content, tags) VALUES (1, 'Quarkus', 'Supersonic Subatomic Java', ARRAY['Java', 'Framework']);
INSERT INTO post(id, title, content, tags) VALUES (2, 'Hibernate', 'Everything Data',  ARRAY['Library']);
INSERT INTO post(id, title, content, tags) VALUES (3, 'Reactive', 'Reactive Application on The JVM',  ARRAY['Java']);

ALTER TABLE post ALTER COLUMN id SET DEFAULT nextval('post_id_seq');

INSERT INTO tag(id, label) VALUES (1, 'Java');
INSERT INTO tag(id, label) VALUES (2, 'Framework');
INSERT INTO tag(id, label) VALUES (3, 'Library');

ALTER TABLE tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq');