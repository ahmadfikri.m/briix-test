package org.acme;

import javax.persistence.*;
import io.smallrye.common.constraint.NotNull;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.Tuple;

@Entity
@NamedQuery(name = "Tags.findAll", query = "SELECT c FROM Tag c ORDER BY c.label")
public class Tag {

    @Id
    @SequenceGenerator(name = "tagSequence", sequenceName = "tag_id_seq", allocationSize = 1, initialValue = 10)
    @GeneratedValue(generator = "tagSequence")
    private Integer id;

    @NotNull
    private String label;

    public Tag() {
    }

    public Tag(Integer id, String label) {
        this.id = id;
        this.label = label;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    private static Tag from(Row row) {
        return new Tag(row.getInteger("id"), row.getString("label"));
    }

    public static Multi<Tag> findAllByTags(PgPool client, String[] labels) {
        return client.preparedQuery("SELECT id, label FROM tag WHERE label = ANY($1)")
            .execute(Tuple.of(labels))
            .onItem().transformToMulti(tags -> Multi.createFrom().iterable(tags))
            .onItem().transform(Tag::from);
    }

    public Uni<Long> create(PgPool client) {
        return client.preparedQuery("INSERT INTO tag (id, label) VALUES (default, $1) RETURNING id")
            .execute(Tuple.of(label))
            .onItem().transform(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }
}
