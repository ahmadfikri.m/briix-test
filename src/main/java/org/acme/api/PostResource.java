package org.acme.api;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.hibernate.reactive.mutiny.Mutiny;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse.Status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;

import org.acme.*;

@Path("posts")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class PostResource {

    private static final Logger LOGGER = Logger.getLogger(PostResource.class);

    @Inject
    Mutiny.SessionFactory sf;

    @Inject
    EntityManager em;

    @Inject
    io.vertx.mutiny.pgclient.PgPool client;

    @GET
    public Multi<PostDto> getListPost() {
        return PostDto.findAll(client);
    }

    @GET
    @Path("{id}")
    public Uni<PostDto> getPostById(Long id) {
        return PostDto.findById(client, id);
    }

    @POST
    public Uni<Response> create(PostDto postDto) {
        if (postDto == null || postDto.getTitle() == null || postDto.getContent() == null) {
            throw new WebApplicationException("Label wasn't invalidly set on request.", 422);
        }

        return postDto.create(client)
            .onItem().transform(id -> {
                postDto.setId(id);

                Tag.findAllByTags(client, postDto.tags)
                    .collect()
                    .asList()
                    .onItem()
                    .transform(tags -> {
                        if(postDto.tags.length > tags.size()) {
                            for (String tagLabel : postDto.tags) {
                                boolean notPresent = !tags.stream()
                                    .filter(rowTag -> tagLabel.equals(rowTag.getLabel()))
                                    .findAny()
                                    .isPresent();

                                if(notPresent) {
                                    Tag newTag = new Tag();
                                    newTag.setLabel(tagLabel);
                                    newTag.create(client).subscribeAsCompletionStage();
                                }
                            }
                        }

                        return tags;
                    })
                    .subscribeAsCompletionStage();

                return Response.ok(postDto).status(Status.CREATED).build();
            });
    }

    @PUT
    @Path("{id}")
    public Uni<Response> update(@RestPath Long id, PostDto postDto) {
        if (postDto == null || postDto.getTitle() == null || postDto.getContent() == null) {
            throw new WebApplicationException("Label wasn't invalidly set on request.", 422);
        }

        return postDto.update(client, id)
            .onItem().transform(updated -> updated ? Status.OK : Status.NOT_FOUND)
            .onItem().transform(status -> {
                postDto.setId(id);

                Tag.findAllByTags(client, postDto.tags)
                    .collect()
                    .asList()
                    .onItem()
                    .transform(tags -> {
                        if(postDto.tags.length > tags.size()) {
                            for (String tagLabel : postDto.tags) {
                                boolean notPresent = !tags.stream()
                                    .filter(rowTag -> tagLabel.equals(rowTag.getLabel()))
                                    .findAny()
                                    .isPresent();

                                if(notPresent) {
                                    Tag newTag = new Tag();
                                    newTag.setLabel(tagLabel);
                                    newTag.create(client).subscribeAsCompletionStage();
                                }
                            }
                        }

                        return tags;
                    })
                    .subscribeAsCompletionStage();

                return Response.ok(postDto).status(status).build();
            });
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(Long id) {
        return PostDto.delete(client, id)
            .onItem().transform(deleted -> deleted ? Status.NO_CONTENT : Status.NOT_FOUND)
            .onItem().transform(status -> Response.status(status).build());
    }

    /**
     * Create a HTTP response from an exception.
     *
     * Response Example:
     *
     * <pre>
     * HTTP/1.1 422 Unprocessable Entity
     * Content-Length: 111
     * Content-Type: application/json
     *
     * {
     *     "code": 422,
     *     "error": "Fruit name was not set on request.",
     *     "exceptionType": "javax.ws.rs.WebApplicationException"
     * }
     * </pre>
     */
    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {
        @Inject
        ObjectMapper objectMapper;

        @Override
        public Response toResponse(Exception exception) {
            LOGGER.error("Failed to handle request", exception);

            int code = 500;

            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }

            ObjectNode exceptionJson = objectMapper.createObjectNode();
            exceptionJson.put("exceptionType", exception.getClass().getName());
            exceptionJson.put("code", code);

            if (exception.getMessage() != null) {
                exceptionJson.put("error", exception.getMessage());
            }

            return Response.status(code)
                .entity(exceptionJson)
                .build();
        }
    }
}
