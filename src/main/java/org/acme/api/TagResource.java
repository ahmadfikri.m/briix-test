package org.acme.api;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.hibernate.reactive.mutiny.Mutiny;

import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse.Status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import io.smallrye.mutiny.Uni;

import org.acme.*;

@Path("tags")
@ApplicationScoped
@Produces("application/json")
@Consumes("application/json")
public class TagResource {

    private static final Logger LOGGER = Logger.getLogger(TagResource.class);

    @Inject
    Mutiny.SessionFactory sf;

    @GET
    public Uni<List<Tag>> get() {
        return sf.withTransaction((s,t) -> s
            .createNamedQuery("Tags.findAll", Tag.class)
            .getResultList()
        );
    }

    @GET
    @Path("{id}")
    public Uni<Tag> getSingle(@RestPath Integer id) {
        return sf.withTransaction((s,t) -> s.find(Tag.class, id));
    }

    @POST
    public Uni<Response> create(Tag tag) {
        if (tag == null || tag.getLabel() == null) {
            throw new WebApplicationException("Label wasn't invalidly set on request.", 422);
        }

        return sf.withTransaction((s,t) -> s.persist(tag))
            .replaceWith(() -> Response.ok(tag).status(Status.CREATED).build());
    }

    @PUT
    @Path("{id}")
    public Uni<Response> update(@RestPath Integer id, Tag tag) {
        if (tag == null || tag.getLabel() == null) {
            throw new WebApplicationException("Label wasn't invalidly set on request.", 422);
        }

        return sf.withTransaction((s,t) -> s.find(Tag.class, id)
            // If entity exists then update it
            .onItem().ifNotNull()
                .invoke(entity -> entity.setLabel(tag.getLabel()))
            // If entity updated return the appropriate response
            .onItem().ifNotNull()
                .transform(entity -> Response.ok(entity).build())
            // If entity not found return the appropriate response
            .onItem().ifNull()
                .continueWith(() -> Response.ok().status(Status.NOT_FOUND).build())
        );
    }

    @DELETE
    @Path("{id}")
    public Uni<Response> delete(@RestPath Integer id) {
        return sf.withTransaction((s,t) ->
            s.find(Tag.class, id)
            // If entity exists then delete it and return the appropriate response
            .onItem().ifNotNull()
                .transformToUni(entity -> s.remove(entity)
                .replaceWith(() -> Response.ok().status(Status.NO_CONTENT).build()))
            // If entity not found return the appropriate response
            .onItem().ifNull()
                .continueWith(() -> Response.ok().status(Status.NOT_FOUND).build()));
    }

    /**
     * Create a HTTP response from an exception.
     *
     * Response Example:
     *
     * <pre>
     * HTTP/1.1 422 Unprocessable Entity
     * Content-Length: 111
     * Content-Type: application/json
     *
     * {
     *     "code": 422,
     *     "error": "Tag label was not set on request.",
     *     "exceptionType": "javax.ws.rs.WebApplicationException"
     * }
     * </pre>
     */
    @Provider
    public static class ErrorMapper implements ExceptionMapper<Exception> {
        @Inject
        ObjectMapper objectMapper;

        @Override
        public Response toResponse(Exception exception) {
            LOGGER.error("Failed to handle request", exception);

            int code = 500;

            if (exception instanceof WebApplicationException) {
                code = ((WebApplicationException) exception).getResponse().getStatus();
            }

            ObjectNode exceptionJson = objectMapper.createObjectNode();
            exceptionJson.put("exceptionType", exception.getClass().getName());
            exceptionJson.put("code", code);

            if (exception.getMessage() != null) {
                exceptionJson.put("error", exception.getMessage());
            }

            return Response.status(code)
                .entity(exceptionJson)
                .build();
        }
    }
}
