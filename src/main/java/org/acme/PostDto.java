package org.acme;

import javax.persistence.*;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.vertx.mutiny.pgclient.PgPool;
import io.vertx.mutiny.sqlclient.Row;
import io.vertx.mutiny.sqlclient.RowSet;
import io.vertx.mutiny.sqlclient.Tuple;

public class PostDto {

    @Id
    @SequenceGenerator(name = "postSequence", sequenceName = "post_id_seq", allocationSize = 1, initialValue = 10)
    @GeneratedValue(generator = "postSequence")
    public Long id;
    public String title;
    public String content;
    public String[] tags;

    public PostDto() {

    }

    public PostDto(Long id, String title, String content, String[] tags) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.tags = tags;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }

    private static PostDto from(Row row) {
        return new PostDto(row.getLong("id"), row.getString("title"), row.getString("content"), row.getArrayOfStrings("tags"));
    }

    public static Multi<PostDto> findAll(PgPool client) {
        return client.query("SELECT id, title, content, tags FROM post")
            .execute()
            .onItem().transformToMulti(posts -> Multi.createFrom().iterable(posts))
            .onItem().transform(PostDto::from);
    }

    public static Uni<PostDto> findById(PgPool client, Long id) {
        return client.preparedQuery("SELECT id, title, content, tags FROM post WHERE id = $1")
          .execute(Tuple.of(id))
          .onItem().transform(RowSet::iterator)
          .onItem().transform(iterator -> iterator.hasNext() ? from(iterator.next()) : null);
    }

    public Uni<Long> create(PgPool client) {
        return client.preparedQuery("INSERT INTO post (id, title, content, tags) VALUES (default, $1, $2, $3) RETURNING id").execute(Tuple.of(title, content, tags))
                .onItem().transform(pgRowSet -> pgRowSet.iterator().next().getLong("id"));
    }

    public Uni<Boolean> update(PgPool client, Long id) {
        return client.preparedQuery("UPDATE post SET title = $1, content = $2, tags = $3 WHERE id = $4")
            .execute(Tuple.of(title, content, tags, id))
            .onItem()
            .transform(pgRowSet -> pgRowSet.rowCount() == 1);
    }

    public static Uni<Boolean> delete(PgPool client, Long id) {
        return client.preparedQuery("DELETE FROM post WHERE id = $1").execute(Tuple.of(id))
                .onItem().transform(pgRowSet -> pgRowSet.rowCount() == 1);
    }
}
