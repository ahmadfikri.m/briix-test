package org.acme;

import javax.persistence.*;
import io.smallrye.common.constraint.NotNull;

@Entity
public class Post {

    @Id
    @SequenceGenerator(name = "postSequence", sequenceName = "post_id_seq", allocationSize = 1, initialValue = 10, catalog = "post")
    @GeneratedValue(generator = "postSequence")
    private Long id;

    @NotNull
    private String title;

    @NotNull
    private String content;

    @Column(columnDefinition = "varchar[]", nullable = true)
    private String[] tags;

    public Post() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String[] getTags() {
        return tags;
    }

    public void setTags(String[] tags) {
        this.tags = tags;
    }
}
